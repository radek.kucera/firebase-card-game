import firebase from "firebase";

const ZAPISDODATABAZE = "ZAPISDODATABAZE";

var config = {
    apiKey: "AIzaSyAAfAMfJ2-z86s5B-l86dujf3omc4_iwHs",
    authDomain: "dirtyshoe-44dbf.firebaseapp.com",
    databaseURL: "https://dirtyshoe-44dbf.firebaseio.com",
    projectId: "dirtyshoe-44dbf",
    storageBucket: "dirtyshoe-44dbf.appspot.com",
    messagingSenderId: "91209852262"
};
  
var db = firebase.initializeApp(config);

  
const initialState = { Firebase: db };
 
export const actionCreators = {
    zapisDoDatabaze: (Firebase) => {
        return { type: ZAPISDODATABAZE, payload: Firebase };
    }
}
 
export const reducer = (state, action) => {
    state = state || initialState;
    switch (action.type) {
        case ZAPISDODATABAZE: {
            return { ...state, Firebase: action.payload };
        }
        default: return state;
    }
}
