const ZMENAKARET = "ZMENAKARET";

const initialState = {
  Cards: [
    {
      id: 0,
      name: "zuzanka",
      mana: 2,
      hp: 1,
      dmg: 2,
      poision: true,
      taunt: false,
      burn: 0,
      image: "./cards/zuzanka.jpg"
    },
    {
      id: 1,
      name: "diabetes",
      mana: 4,
      hp: 1,
      dmg: 5,
      poision: false,
      taunt: false,
      burn: 0,
      image: "./cards/diabetes.jpg"
    },
    {
      id: 2,
      name: "general_kitt",
      mana: 3,
      hp: 2,
      dmg: 4,
      poision: false,
      taunt: false,
      burn: 0,
      image: "./cards/general_kitt.jpg"
    },
    {
      id: 3,
      name: "general_katt",
      mana: 3,
      hp: 4,
      dmg: 2,
      poision: false,
      taunt: false,
      burn: 0,
      image: "./cards/general_katt.jpg"
    },
    {
      id: 4,
      name: "kral_zlatomil",
      mana: 1,
      hp: 2,
      dmg: 2,
      poision: false,
      taunt: false,
      burn: 0,
      image: "./cards/kral_zlatomil.jpg"
    },
    {
      id: 5,
      name: "nocenka",
      mana: 4,
      hp: 3,
      dmg: 4,
      poision: false,
      taunt: false,
      burn: 0,
      image: "./cards/nocenka.jpg"
    },
    {
      id: 6,
      name: "prekvapeni",
      mana: 5,
      hp: 2,
      dmg: 2,
      poision: true,
      taunt: false,
      burn: 0,
      image: "./cards/prekvapeni.jpg"
    },
    {
      id: 7,
      name: "robobob",
      mana: 2,
      hp: 1,
      dmg: 3,
      poision: false,
      taunt: true,
      burn: 0,
      image: "./cards/robobob.jpg"
    },
    {
      id: 8,
      name: "samal",
      mana: 3,
      hp: 2,
      dmg: 2,
      poision: false,
      taunt: false,
      burn: 2,
      image: "./cards/samal.jpg"
    },
    {
      id: 9,
      name: "tlukohlav",
      mana: 4,
      hp: 5,
      dmg: 2,
      poision: false,
      taunt: true,
      burn: 0,
      image: "./cards/tlukohlav.jpg"
    }
  ]
};

export const actionCreators = {
  zmenaKaret: Cards => {
    return { type: ZMENAKARET, payload: Cards };
  }
};

export const reducer = (state, action) => {
  state = state || initialState;
  switch (action.type) {
    case ZMENAKARET: {
      return { ...state, Cards: action.payload };
    }
    default:
      return state;
  }
};
