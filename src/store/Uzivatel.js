const NASTAVUZIVATELE = "NASTAVUZIVATELE";

const initialState = { Uzivatel: null };
 
export const actionCreators = {
    nastavUzivatele: (Uzivatel) => {
        return { type: NASTAVUZIVATELE, payload: Uzivatel };
    }
}
 
export const reducer = (state, action) => {
    state = state || initialState;
    switch (action.type) {
        case NASTAVUZIVATELE: {
            return { ...state, Uzivatel: action.payload };
        }
        default: return state;
    }
}