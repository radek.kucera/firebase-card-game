const ZMENAHRY = "ZMENAHRY";

const initialState = { GameInfo: {
    game: null,
    enemygame: null,
    exist: false
}};
 
export const actionCreators = {
    zmenaHry: (GameInfo) => {
        return { type: ZMENAHRY, payload: GameInfo };
    }
}
 
export const reducer = (state, action) => {
    state = state || initialState;
    switch (action.type) {
        case ZMENAHRY: {
            return { ...state, GameInfo: action.payload };
        }
        default: return state;
    }
}
