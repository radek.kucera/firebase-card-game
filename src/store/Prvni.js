const ZMENAPRVNIHO = "ZMENAPRVNIHO";

const initialState = { Prvni: ""};
 
export const actionCreators = {
    zmenaPrvniho: (Prvni) => {
        return { type: ZMENAPRVNIHO, payload: Prvni };
    }
}
 
export const reducer = (state, action) => {
    state = state || initialState;
    switch (action.type) {
        case ZMENAPRVNIHO: {
            return { ...state, Prvni: action.payload };
        }
        default: return state;
    }
}
