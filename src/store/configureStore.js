import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import * as Prvni from "./Prvni";
import * as Firebase from "./Firebase";
import * as Uzivatel from "./Uzivatel";
import * as GameInfo from "./GameInfo";
import * as Cards from "./Cards";

export default function configureStore(initialState) {
  const middleware = [];
  const enhancers = [];
  const rootReducer = () =>
    combineReducers({
      Prvni: Prvni.reducer,
      Firebase: Firebase.reducer,
      Uzivatel: Uzivatel.reducer,
      GameInfo: GameInfo.reducer,
      Cards: Cards.reducer
    });

  return createStore(
    rootReducer(),
    initialState,
    compose(
      applyMiddleware(...middleware),
      ...enhancers
    )
  );
}
