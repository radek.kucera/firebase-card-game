import React, { Component } from "react";
import "./App.css";
import { Provider } from "react-redux";
//? import { Switch, Router, Route } from 'react-router-dom';
import configureStore from "./store/configureStore";
import Board from "./Components/Board/Board";
import ReactOrientation from "react-orientation";
import Portrait from "./Components/Portrait/Portrait";
import { Offline, Online } from "react-detect-offline";
import Loading from "./Components/Loading/Loading";
// TODO offline component
const initialState = {};

const store = configureStore(initialState);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Offline>
          <Loading stav="Nejste připojen k síti..." />
        </Offline>
        <ReactOrientation type="landscape">
          <Portrait />
        </ReactOrientation>
        <Online>
          <Board />
        </Online>
      </Provider>
    );
  }
}

export default App;
