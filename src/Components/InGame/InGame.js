import React, { Component } from "react";
import "./InGame.css";
import firebase from "firebase";
import { actionCreators as acGameInfo } from "../../store/GameInfo";
import { actionCreators as acCards } from "../../store/Cards";
import { connect } from "react-redux";
import Loading from "../Loading/Loading";
import Enemy from "../Enemy/Enemy";
import You from "../You/You";
// ? const Enemy = React.lazy(() => import("../Enemy/Enemy"));
// ? const You = React.lazy(() => import("../You/You"));

class InGame extends Component {
  constructor() {
    super();
    this.getGameData = this.getGameData.bind(this);
    this.state = {
      data: null,
      status: false,
      snapshot: null
    };
  }
  getGameData() {
    this.setState({
      data: firebase
        .database()
        .ref("games")
        .child(this.props.GameInfo.game),
      status: true
    });
  }

  render() {
    if (this.state.status === false) {
      this.getGameData();
      return <Loading stav="Hra se vytváří..." />;
    } else {
      var data;
      this.state.data.once("value", snapshot => {
        data = snapshot.val();
      });
      var start = false;
      if (data.players.Prvni.id > data.players.Druhy.id) {
        start = true;
        // ! JSTE PRVNÍ !
      }
      return (
        <div className="in-game">
          <div className="you">
            <span className="player-info">
              <img
                src={data.players.Prvni.picture}
                alt={data.players.Prvni.name}
              />
              <p>{data.players.Prvni.name}</p>
            </span>
            <You />
          </div>
          <div className="enemy">
            <span className="player-info">
              <p>{data.players.Druhy.name}</p>
              <img
                src={data.players.Druhy.picture}
                alt={data.players.Druhy.name}
              />
            </span>
            <Enemy />
          </div>
        </div>
      );
    }
  }
}

export default connect(
  state => {
    return {
      GameInfo: state.GameInfo.GameInfo,
      Cards: state.Cards.Cards
    };
  },
  {
    zmenaHry: acGameInfo.zmenaHry,
    zmenaKaret: acCards.zmenaKaret
  }
)(InGame);
