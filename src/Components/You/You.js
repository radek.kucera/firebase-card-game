import React, { Component } from "react";
import "./You.css";
import { actionCreators as acCards } from "../../store/Cards";
import { connect } from "react-redux";

class You extends Component {
  render() {
    return (
      <>
        <div className="meter hp">
          <span />
        </div>
        <div className="meter mana">
          <span />
        </div>
        <div className="cards">
          <img
            className="card"
            src={this.props.Cards[0].image}
            alt={this.props.Cards[0].name}
          />
          <img
            className="card"
            src={this.props.Cards[1].image}
            alt={this.props.Cards[1].name}
          />
          <img
            className="card"
            src={this.props.Cards[2].image}
            alt={this.props.Cards[2].name}
          />
          <img
            className="card"
            src={this.props.Cards[3].image}
            alt={this.props.Cards[3].name}
          />
        </div>
      </>
    );
  }
}

export default connect(
  state => {
    return {
      Cards: state.Cards.Cards
    };
  },
  {
    zmenaKaret: acCards.zmenaKaret
  }
)(You);
