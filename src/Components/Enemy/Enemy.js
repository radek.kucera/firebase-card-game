import React, { Component } from "react";
import "./Enemy.css";
import { actionCreators as acCards } from "../../store/Cards";
import { connect } from "react-redux";

class Enemy extends Component {
  render() {
    return (
      <>
        <div className="enemy-meter hp">
          <span />
        </div>
        <div className="enemy-meter mana">
          <span />
        </div>
        <div className="enemy-cards">
          <img
            className="enemy-card"
            src="./cards/cardback.png"
            alt="cardback"
          />
          <img
            className="enemy-card"
            src="./cards/cardback.png"
            alt="cardback"
          />
          <img
            className="enemy-card"
            src="./cards/cardback.png"
            alt="cardback"
          />
          <img
            className="enemy-card"
            src="./cards/cardback.png"
            alt="cardback"
          />
        </div>
      </>
    );
  }
}

export default connect(
  state => {
    return {
      Cards: state.Cards.Cards
    };
  },
  {
    zmenaKaret: acCards.zmenaKaret
  }
)(Enemy);
